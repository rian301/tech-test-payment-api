﻿namespace Pottencial.Business.Enums
{
    public enum VendaEnum
    {
        AguardandoPagamento = 1,
        PagamentoAprovado = 2,
        Cancelada = 3,
        EnviadoParaTransportadora = 4,
        Entregue = 5,
    }
}
