﻿using System;
using System.Threading.Tasks;
using Pottencial.Business.Enums;
using Pottencial.Business.Intefaces;
using Pottencial.Business.Models;
using Pottencial.Business.Models.Validations;

namespace Pottencial.Business.Services
{
    public class VendaService : BaseService, IVendaService
    {
        private readonly IVendaRepository _vendaRepository;

        public VendaService(IVendaRepository vendaRepository,
                              INotificador notificador) : base(notificador)
        {
            _vendaRepository = vendaRepository;
        }

        public async Task Adicionar(Venda venda)
        {
            if (!ExecutarValidacao(new VendaValidation(), venda)) return;

            await _vendaRepository.Adicionar(venda);
        }

        public async Task<bool> Atualizar(Venda venda)
        {
            if (!ExecutarValidacao(new VendaValidation(), venda)) return false;

            var result = await VerificaStatus(venda.Status, venda.Id);

            if (!result) return false;

            else
            {
                await _vendaRepository.Atualizar(venda);
                return true;
            }
        }

        public async Task Remover(Guid id)
        {
            await _vendaRepository.Remover(id);
        }

        public void Dispose()
        {
            _vendaRepository?.Dispose();
        }

        private async Task<bool> VerificaStatus(VendaEnum status, Guid id)
        {
            var venda = await _vendaRepository.ObterPorId(id);

            if (venda.Status == VendaEnum.AguardandoPagamento && status == VendaEnum.PagamentoAprovado)
            {
                venda.Status = status;
                return true;
            }
            else if (venda.Status == VendaEnum.AguardandoPagamento && status == VendaEnum.Cancelada)
            {
                venda.Status = status;
                return true;
            }

            else if (venda.Status == VendaEnum.PagamentoAprovado && status == VendaEnum.EnviadoParaTransportadora)
            {
                venda.Status = status;
                return true;
            }

            else if (venda.Status == VendaEnum.PagamentoAprovado && status == VendaEnum.Cancelada)
            {
                venda.Status = status;
                return true;
            }
            else if (venda.Status == VendaEnum.EnviadoParaTransportadora && status == VendaEnum.Entregue)
            {
                venda.Status = status;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}