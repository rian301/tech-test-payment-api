﻿using Pottencial.Business.Models;

namespace Pottencial.Business.Intefaces
{
    public interface IVendaService : IDisposable
    {
        Task Adicionar(Venda venda);
        Task<bool> Atualizar(Venda venda);
        Task Remover(Guid id);
    }
}