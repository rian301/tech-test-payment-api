﻿
using Pottencial.Business.Enums;
using Pottencial.Business.Models.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pottencial.Business.Models
{
    public class Venda : Entity
    {
        public string VendedorId { get; set; }
        public DateTime DataVenda { get; set; }
        public int PedidoId { get; set; }
        public VendaEnum Status { get; set; }
        public string Itens { get; set; }
    }
}
