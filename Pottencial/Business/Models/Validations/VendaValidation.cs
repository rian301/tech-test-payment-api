﻿using FluentValidation;
using Pottencial.Business.Enums;
using Pottencial.Business.Intefaces;

namespace Pottencial.Business.Models.Validations
{
    public class VendaValidation : AbstractValidator<Venda>
    {
        public VendaValidation()
        {
            RuleFor(c => c.VendedorId)
                .NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido")
                .Length(1, 200).WithMessage("O campo {PropertyName} precisa ter entre {MinLength} e {MaxLength} caracteres");

            RuleFor(c => c.Itens)
                .NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido. É necessário pelo menos um item na venda.")
                .WithMessage("O campo {PropertyName} precisa ter entre {MinLength} e {MaxLength} caracteres");

            RuleFor(c => c.Status)
                .NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido");
        }
    }
}