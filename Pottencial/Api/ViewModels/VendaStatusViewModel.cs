﻿using Pottencial.Business.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Api.ViewModels
{
    public class VendaStatusViewModel
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public VendaEnum Status { get; set; }

    }
}