﻿using Pottencial.Business.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Api.ViewModels
{
    public class VendaViewModel
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [StringLength(200, ErrorMessage = "O campo {0} precisa ter entre {1} e {2} caracteres", MinimumLength = 1)]
        public string VendedorId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [StringLength(1000, ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", MinimumLength = 2)]
        public string DataVenda { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public decimal PedidoId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public VendaEnum Status { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string Itens { get; set; }
    }
}