﻿using Microsoft.Extensions.Options;
using Pottencial.Business.Intefaces;
using Pottencial.Business.Notificacoes;
using Pottencial.Business.Services;
using Pottencial.Data.Context;
using Pottencial.Data.Repository;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Api.Configuration
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            services.AddScoped<MeuDbContext>();

            services.AddScoped<IVendaRepository, VendaRepository>();

            services.AddScoped<INotificador, Notificador>();
            services.AddScoped<IVendaService, VendaService>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();

            return services;
        }
    }
}