﻿using AutoMapper;
using Pottencial.Api.ViewModels;
using Pottencial.Business.Models;

namespace Api.Configuration
{
    public class AutomapperConfig : Profile
    {
        public AutomapperConfig()
        {
            CreateMap<Venda, VendaViewModel>().ReverseMap();
        }
    }
}