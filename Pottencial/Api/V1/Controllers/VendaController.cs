﻿using Api.Controllers;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Pottencial.Api.ViewModels;
using Pottencial.Business.Intefaces;
using Pottencial.Business.Models;

namespace Api.V1.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/vendas")]
    public class VendaController : MainController
    {
        private readonly IVendaRepository _vendaRepository;
        private readonly IVendaService _vendaService;
        private readonly IMapper _mapper;

        public VendaController(INotificador notificador,
                                  IVendaRepository vendaRepository,
                                  IVendaService vendaService,
                                  IMapper mapper) : base(notificador)
        {
            _vendaRepository = vendaRepository;
            _vendaService = vendaService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<VendaViewModel>> ObterTodos()
        {
            return _mapper.Map<IEnumerable<VendaViewModel>>(await _vendaRepository.ObterTodos());
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<VendaViewModel>> ObterPorId(Guid id)
        {
            var vendaViewModel = await ObterVenda(id);

            if (vendaViewModel == null) return NotFound();

            return vendaViewModel;
        }

        [HttpPost]
        public async Task<ActionResult<VendaViewModel>> Adicionar(VendaViewModel vendaViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            await _vendaService.Adicionar(_mapper.Map<Venda>(vendaViewModel));

            return CustomResponse(vendaViewModel);
        }

        //[HttpPut("{id:guid}")]
        //public async Task<IActionResult> Atualizar(Guid id, VendaViewModel vendaViewModel)
        //{
        //    if (id != vendaViewModel.Id)
        //    {
        //        NotificarErro("Os ids informados não são iguais!");
        //        return CustomResponse();
        //    }

        //    var vendaAtualizacao = await ObterVenda(id);
        //    if (!ModelState.IsValid) return CustomResponse(ModelState);

        //    vendaAtualizacao.VendedorId = vendaViewModel.VendedorId;
        //    vendaAtualizacao.DataVenda = vendaViewModel.DataVenda;
        //    vendaAtualizacao.PedidoId = vendaViewModel.PedidoId;
        //    vendaAtualizacao.Status = vendaViewModel.Status;

        //    await _vendaService.Atualizar(_mapper.Map<Venda>(vendaAtualizacao));

        //    return CustomResponse(vendaViewModel);
        //}

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Atualizar(Guid id, VendaStatusViewModel vendaStatusViewModel)
        {
            var vendaAtualizacao = await ObterVenda(id);
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            vendaAtualizacao.Status = vendaStatusViewModel.Status;

            var ret = await _vendaService.Atualizar(_mapper.Map<Venda>(vendaAtualizacao));

            if (ret)
                return CustomResponse("Status atualizado com sucesso!");
            return BadRequest("O Status informado não é permitido!");
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<VendaViewModel>> Excluir(Guid id)
        {
            var venda = await ObterVenda(id);

            if (venda == null) return NotFound();

            await _vendaService.Remover(id);

            return CustomResponse(venda);
        }

        private async Task<VendaViewModel> ObterVenda(Guid id)
        {
            var ret = await _vendaRepository.ObterPorId(id);
            return _mapper.Map<VendaViewModel>(ret);
        }
    }
}