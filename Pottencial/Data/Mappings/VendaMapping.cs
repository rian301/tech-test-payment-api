﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pottencial.Business.Models;
using System.Reflection.Emit;

namespace Loja.Data.Mappings
{
    public class VendaMapping : IEntityTypeConfiguration<Venda>
    {
        public void Configure(EntityTypeBuilder<Venda> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.VendedorId)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.Property(p => p.DataVenda)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.Property(p => p.PedidoId)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.Property(p => p.Status)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.Property(p => p.Itens)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.ToTable("Vendas");
        }
    }
}