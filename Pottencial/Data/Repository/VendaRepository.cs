﻿using Pottencial.Business.Intefaces;
using Pottencial.Business.Models;
using Pottencial.Data.Context;

namespace Pottencial.Data.Repository
{
    public class VendaRepository : Repository<Venda>, IVendaRepository
    {
        public VendaRepository(MeuDbContext context) : base(context) { }

    }
}